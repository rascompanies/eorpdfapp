﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using Microsoft.SqlServer.Server;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Web;

namespace EOR_PDF
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] files = Directory.GetFiles(@"\\rasnt6\\apps\\EOR\\");
            foreach (string file in files)
            {
                FileInfo fInfo = new FileInfo(file);
                if (fInfo.LastWriteTime < DateTime.Now.AddDays(-5))
                {
                    fInfo.Delete();
                }
            }

            SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["RASWarehouse"].ConnectionString );
            SqlDataReader sqlDataReader = null;
            
            try
            {
                sqlCon.Open();
                SqlCommand sqlComm = new SqlCommand("EXEC iVOSEORImageCapture", sqlCon);
                sqlComm.CommandTimeout = 300;

                sqlDataReader = sqlComm.ExecuteReader();

                DataTable dTable = new DataTable("EORIndex");
                dTable.Load(sqlDataReader);

                MemoryStream combineStream = new MemoryStream();
                PdfCopyFields copy = new PdfCopyFields(combineStream);

                int i = 1;
                string folderName = DateTime.Now.ToShortDateString().Replace("/", "");
                System.IO.Directory.CreateDirectory(@"\\rasnt6\\apps\\EOR\\" + folderName);
                string clientName = "";
                DateTime cDate = DateTime.Today;
                if( dTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dTable.Rows)
                    {
                        string eorLocation = row.ItemArray[17].ToString();
                        clientName = row.ItemArray[24].ToString();

                        //System.IO.File.Copy(eorLocation, @"\\rasnt6\\apps\\EOR\\EORs_TEST_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".PDF");

                        string eorFileName = row.ItemArray[03].ToString() + "_" + row.ItemArray[0].ToString() + "_" + row.ItemArray[9].ToString() + "_" + Convert.ToString(i) + ".pdf";

                        // creation of the document with a certain size and certain margins
                        iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 0, 0, 0, 0);

                        // creation of the different writers
                        PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, new System.IO.FileStream(@"\\rasnt6\\apps\\EOR\\" + folderName + "\\" + eorFileName, System.IO.FileMode.Create));

                        Bitmap bitMap = new Bitmap(eorLocation);
                        int total = bitMap.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page);

                        //FileStream fs = new FileStream(@"C:\\EOR_Test\\TestFile.PDF", FileMode.Create);
                        document.Open();

                        iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;

                        for (int k = 0; k < total; ++k)
                        {
                            bitMap.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Page, k);

                            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bitMap, System.Drawing.Imaging.ImageFormat.Tiff);
                            img.SetAbsolutePosition(0, 0);
                            img.ScaleAbsoluteHeight(document.PageSize.Height);
                            img.ScaleAbsoluteWidth(document.PageSize.Width);
                            cb.AddImage(img);

                            iTextSharp.text.Phrase myText;

                            ColumnText ct = new ColumnText(cb);
                            if (total > 1)
                            {
                                myText = new iTextSharp.text.Phrase("Check Amount: " + string.Format("{0:#.00}", Convert.ToDecimal(row.ItemArray[21])) + " - " + row.ItemArray[22].ToString() + " (" + (k + 1).ToString() + "/" + total.ToString() + ")" + "/" + row.ItemArray[23].ToString());
                            }
                            else
                            {
                                myText = new iTextSharp.text.Phrase("Check Amount: " + string.Format("{0:#.00}", Convert.ToDecimal(row.ItemArray[21])) + " - " + row.ItemArray[22].ToString() + "/" + row.ItemArray[23].ToString());
                            }

                            ct.SetSimpleColumn(myText, document.PageSize.Width - 200, document.PageSize.Height - 60, document.PageSize.Width, document.PageSize.Height, 15, iTextSharp.text.Element.ALIGN_RIGHT);
                            ct.Go();
                            document.NewPage();
                        }

                        document.Close();

                        var ms1 = new MemoryStream(System.IO.File.ReadAllBytes(@"\\rasnt6\\apps\\EOR\\" + folderName + "\\" + eorFileName));
                        ms1.Position = 0;
                        copy.AddDocument(new PdfReader(ms1));
                        ms1.Dispose();

                        i++;
                    }

                    copy.Close();

                    if (clientName == "State of South Dakota")
                    {
                        System.IO.File.WriteAllBytes(@"\\rasnt6\\apps\\EOR\\EORs_Combined_StateOfSouthDakota_" + DateTime.Now.ToShortDateString().Replace("/", "") + "2.PDF", combineStream.ToArray());
                    }
                    else if (cDate.DayOfWeek == DayOfWeek.Wednesday && clientName != "State of South Dakota")
                    {
                        System.IO.File.WriteAllBytes(@"\\rasnt6\\apps\\EOR\\EORs_Combined_BryanHealth_Madonna_CityOfSF_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".PDF", combineStream.ToArray());
                    }
                    else if (clientName == "Douglas County School District 0001")
                    {
                        System.IO.File.WriteAllBytes(@"\\rasnt6\\apps\\EOR\\EORs_Combined_OmahaPublicSchools_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".PDF", combineStream.ToArray());
                    }
                    else
                    {
                        System.IO.File.WriteAllBytes(@"\\rasnt6\\apps\\EOR\\EORs_Combined_StrataCare_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".PDF", combineStream.ToArray());
                    }

                    //System.IO.File.WriteAllBytes(@"\\rasnt6\\apps\\EOR\\EORs_Combined_" + DateTime.Now.ToLongDateString() + ".PDF",combineStream.ToArray());

                    System.IO.Directory.Delete(@"\\rasnt6\\apps\\EOR\\" + folderName, true);
                }               

            }
            finally
            {
                sqlDataReader.Close();
                sqlCon.Close();
            }

            
        }
    }
}
